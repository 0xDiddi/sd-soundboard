﻿using System;
using System.Collections.Generic;

namespace sd_soundboard.Models {
    public class SoundBoardSettings {
        public float Volume { get; set; } = .5f;

        public string File { get; set; } = null;

        public int Device { get; set; } = -1;

        public Dictionary<int, string> Devices = new Dictionary<int, string>();

        public bool UpdateDevices { get; set; }

        public SoundBoardSettings() {
            var caps = DeviceHelper.GetAudioDevices();
            for (var i = 0; i < caps.Count; i++) Devices.Add(i, caps[i].ProductName);
        }
    }
}