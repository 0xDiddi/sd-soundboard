using System.Collections.Generic;
using System.Runtime.InteropServices;
using NAudio.Wave;

namespace sd_soundboard {
    public static class DeviceHelper {
        [DllImport("winmm.dll", SetLastError = true)]
        private static extern uint waveOutGetNumDevs();

        [DllImport("winmm.dll", SetLastError = true, CharSet = CharSet.Auto)]
        private static extern uint waveOutGetDevCaps(uint hwo, ref WaveOutCapabilities pwoc, uint cbwoc);
        
        public static List<WaveOutCapabilities> GetAudioDevices() {
            var capabilities = new List<WaveOutCapabilities>();

            for (uint i = 0; i < waveOutGetNumDevs(); i++) {
                var caps = new WaveOutCapabilities();

                waveOutGetDevCaps(i, ref caps, (uint) Marshal.SizeOf<WaveOutCapabilities>());
                
                capabilities.Add(caps);
            }

            return capabilities;
        }
    }
}